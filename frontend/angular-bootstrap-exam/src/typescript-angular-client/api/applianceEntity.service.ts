/**
 * REST API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0
 * Contact: bla@bla.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
/* tslint:disable:no-unused-variable member-ordering */

import { Inject, Injectable, Optional }                      from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams,
         HttpResponse, HttpEvent }                           from '@angular/common/http';
import { CustomHttpUrlEncodingCodec }                        from '../encoder';

import { Observable }                                        from 'rxjs';

import { Appliance } from '../model/appliance';
import { CollectionModelAppliance } from '../model/collectionModelAppliance';
import { EntityModelAppliance } from '../model/entityModelAppliance';

import { BASE_PATH, COLLECTION_FORMATS }                     from '../variables';
import { Configuration }                                     from '../configuration';


@Injectable()
export class ApplianceEntityService {

    protected basePath = 'https://192.168.18.4:8001';
    public defaultHeaders = new HttpHeaders();
    public configuration = new Configuration();

    constructor(protected httpClient: HttpClient, @Optional()@Inject(BASE_PATH) basePath: string, @Optional() configuration: Configuration) {
        if (basePath) {
            this.basePath = basePath;
        }
        if (configuration) {
            this.configuration = configuration;
            this.basePath = basePath || configuration.basePath || this.basePath;
        }
    }

    /**
     * @param consumes string[] mime-types
     * @return true: consumes contains 'multipart/form-data', false: otherwise
     */
    private canConsumeForm(consumes: string[]): boolean {
        const form = 'multipart/form-data';
        for (const consume of consumes) {
            if (form === consume) {
                return true;
            }
        }
        return false;
    }


    /**
     * deleteAppliance
     * 
     * @param id id
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public deleteApplianceUsingDELETE(id: number, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public deleteApplianceUsingDELETE(id: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public deleteApplianceUsingDELETE(id: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public deleteApplianceUsingDELETE(id: number, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (id === null || id === undefined) {
            throw new Error('Required parameter id was null or undefined when calling deleteApplianceUsingDELETE.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            '*/*'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.delete<any>(`${this.basePath}/exam/appliances/${encodeURIComponent(String(id))}`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * findAllAppliance
     * 
     * @param page page
     * @param size size
     * @param sort sort
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public findAllApplianceUsingGET(page?: number, size?: number, sort?: string, observe?: 'body', reportProgress?: boolean): Observable<CollectionModelAppliance>;
    public findAllApplianceUsingGET(page?: number, size?: number, sort?: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<CollectionModelAppliance>>;
    public findAllApplianceUsingGET(page?: number, size?: number, sort?: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<CollectionModelAppliance>>;
    public findAllApplianceUsingGET(page?: number, size?: number, sort?: string, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {




        let queryParameters = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()});
        if (page !== undefined && page !== null) {
            queryParameters = queryParameters.set('page', <any>page);
        }
        if (size !== undefined && size !== null) {
            queryParameters = queryParameters.set('size', <any>size);
        }
        if (sort !== undefined && sort !== null) {
            queryParameters = queryParameters.set('sort', <any>sort);
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/hal+json',
            'application/json',
            'application/x-spring-data-compact+json',
            'text/uri-list'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.get<CollectionModelAppliance>(`${this.basePath}/exam/appliances`,
            {
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * findByBrandAppliance
     * 
     * @param brand brand
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public findByBrandApplianceUsingGET(brand?: string, observe?: 'body', reportProgress?: boolean): Observable<CollectionModelAppliance>;
    public findByBrandApplianceUsingGET(brand?: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<CollectionModelAppliance>>;
    public findByBrandApplianceUsingGET(brand?: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<CollectionModelAppliance>>;
    public findByBrandApplianceUsingGET(brand?: string, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {


        let queryParameters = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()});
        if (brand !== undefined && brand !== null) {
            queryParameters = queryParameters.set('brand', <any>brand);
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            '*/*'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.get<CollectionModelAppliance>(`${this.basePath}/exam/appliances/search/findByBrand`,
            {
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * findByDateBoughtAppliance
     * 
     * @param dateBought dateBought
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public findByDateBoughtApplianceUsingGET(dateBought?: Date, observe?: 'body', reportProgress?: boolean): Observable<CollectionModelAppliance>;
    public findByDateBoughtApplianceUsingGET(dateBought?: Date, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<CollectionModelAppliance>>;
    public findByDateBoughtApplianceUsingGET(dateBought?: Date, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<CollectionModelAppliance>>;
    public findByDateBoughtApplianceUsingGET(dateBought?: Date, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {


        let queryParameters = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()});
        if (dateBought !== undefined && dateBought !== null) {
            queryParameters = queryParameters.set('dateBought', <any>dateBought);
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            '*/*'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.get<CollectionModelAppliance>(`${this.basePath}/exam/appliances/search/findByDateBought`,
            {
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * findByIdAppliance
     * 
     * @param id id
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public findByIdApplianceUsingGET(id: number, observe?: 'body', reportProgress?: boolean): Observable<EntityModelAppliance>;
    public findByIdApplianceUsingGET(id: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<EntityModelAppliance>>;
    public findByIdApplianceUsingGET(id: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<EntityModelAppliance>>;
    public findByIdApplianceUsingGET(id: number, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (id === null || id === undefined) {
            throw new Error('Required parameter id was null or undefined when calling findByIdApplianceUsingGET.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            '*/*'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.get<EntityModelAppliance>(`${this.basePath}/exam/appliances/${encodeURIComponent(String(id))}`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * findByModelAppliance
     * 
     * @param model model
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public findByModelApplianceUsingGET(model?: string, observe?: 'body', reportProgress?: boolean): Observable<CollectionModelAppliance>;
    public findByModelApplianceUsingGET(model?: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<CollectionModelAppliance>>;
    public findByModelApplianceUsingGET(model?: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<CollectionModelAppliance>>;
    public findByModelApplianceUsingGET(model?: string, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {


        let queryParameters = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()});
        if (model !== undefined && model !== null) {
            queryParameters = queryParameters.set('model', <any>model);
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            '*/*'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.get<CollectionModelAppliance>(`${this.basePath}/exam/appliances/search/findByModel`,
            {
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * findBySerialNumberAndBrandAndModelAndStatusAndDateBoughtAppliance
     * 
     * @param brand brand
     * @param dateBought dateBought
     * @param model model
     * @param serialNumber serialNumber
     * @param status status
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public findBySerialNumberAndBrandAndModelAndStatusAndDateBoughtApplianceUsingGET(brand?: string, dateBought?: Date, model?: string, serialNumber?: string, status?: string, observe?: 'body', reportProgress?: boolean): Observable<EntityModelAppliance>;
    public findBySerialNumberAndBrandAndModelAndStatusAndDateBoughtApplianceUsingGET(brand?: string, dateBought?: Date, model?: string, serialNumber?: string, status?: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<EntityModelAppliance>>;
    public findBySerialNumberAndBrandAndModelAndStatusAndDateBoughtApplianceUsingGET(brand?: string, dateBought?: Date, model?: string, serialNumber?: string, status?: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<EntityModelAppliance>>;
    public findBySerialNumberAndBrandAndModelAndStatusAndDateBoughtApplianceUsingGET(brand?: string, dateBought?: Date, model?: string, serialNumber?: string, status?: string, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {






        let queryParameters = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()});
        if (brand !== undefined && brand !== null) {
            queryParameters = queryParameters.set('brand', <any>brand);
        }
        if (dateBought !== undefined && dateBought !== null) {
            queryParameters = queryParameters.set('dateBought', <any>dateBought);
        }
        if (model !== undefined && model !== null) {
            queryParameters = queryParameters.set('model', <any>model);
        }
        if (serialNumber !== undefined && serialNumber !== null) {
            queryParameters = queryParameters.set('serialNumber', <any>serialNumber);
        }
        if (status !== undefined && status !== null) {
            queryParameters = queryParameters.set('status', <any>status);
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            '*/*'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.get<EntityModelAppliance>(`${this.basePath}/exam/appliances/search/findBySerialNumberAndBrandAndModelAndStatusAndDateBought`,
            {
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * findBySerialNumberAndBrandAndModelAppliance
     * 
     * @param brand brand
     * @param model model
     * @param serialNumber serialNumber
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public findBySerialNumberAndBrandAndModelApplianceUsingGET(brand?: string, model?: string, serialNumber?: string, observe?: 'body', reportProgress?: boolean): Observable<EntityModelAppliance>;
    public findBySerialNumberAndBrandAndModelApplianceUsingGET(brand?: string, model?: string, serialNumber?: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<EntityModelAppliance>>;
    public findBySerialNumberAndBrandAndModelApplianceUsingGET(brand?: string, model?: string, serialNumber?: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<EntityModelAppliance>>;
    public findBySerialNumberAndBrandAndModelApplianceUsingGET(brand?: string, model?: string, serialNumber?: string, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {




        let queryParameters = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()});
        if (brand !== undefined && brand !== null) {
            queryParameters = queryParameters.set('brand', <any>brand);
        }
        if (model !== undefined && model !== null) {
            queryParameters = queryParameters.set('model', <any>model);
        }
        if (serialNumber !== undefined && serialNumber !== null) {
            queryParameters = queryParameters.set('serialNumber', <any>serialNumber);
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            '*/*'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.get<EntityModelAppliance>(`${this.basePath}/exam/appliances/search/findBySerialNumberAndBrandAndModel`,
            {
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * findBySerialNumberAppliance
     * 
     * @param serialNumber serialNumber
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public findBySerialNumberApplianceUsingGET(serialNumber?: string, observe?: 'body', reportProgress?: boolean): Observable<CollectionModelAppliance>;
    public findBySerialNumberApplianceUsingGET(serialNumber?: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<CollectionModelAppliance>>;
    public findBySerialNumberApplianceUsingGET(serialNumber?: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<CollectionModelAppliance>>;
    public findBySerialNumberApplianceUsingGET(serialNumber?: string, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {


        let queryParameters = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()});
        if (serialNumber !== undefined && serialNumber !== null) {
            queryParameters = queryParameters.set('serialNumber', <any>serialNumber);
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            '*/*'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.get<CollectionModelAppliance>(`${this.basePath}/exam/appliances/search/findBySerialNumber`,
            {
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * findByStatusAppliance
     * 
     * @param status status
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public findByStatusApplianceUsingGET(status?: string, observe?: 'body', reportProgress?: boolean): Observable<CollectionModelAppliance>;
    public findByStatusApplianceUsingGET(status?: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<CollectionModelAppliance>>;
    public findByStatusApplianceUsingGET(status?: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<CollectionModelAppliance>>;
    public findByStatusApplianceUsingGET(status?: string, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {


        let queryParameters = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()});
        if (status !== undefined && status !== null) {
            queryParameters = queryParameters.set('status', <any>status);
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            '*/*'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.get<CollectionModelAppliance>(`${this.basePath}/exam/appliances/search/findByStatus`,
            {
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * saveAppliance
     * 
     * @param body body
     * @param id id
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public saveApplianceUsingPATCH(body: Appliance, id: number, observe?: 'body', reportProgress?: boolean): Observable<EntityModelAppliance>;
    public saveApplianceUsingPATCH(body: Appliance, id: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<EntityModelAppliance>>;
    public saveApplianceUsingPATCH(body: Appliance, id: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<EntityModelAppliance>>;
    public saveApplianceUsingPATCH(body: Appliance, id: number, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (body === null || body === undefined) {
            throw new Error('Required parameter body was null or undefined when calling saveApplianceUsingPATCH.');
        }

        if (id === null || id === undefined) {
            throw new Error('Required parameter id was null or undefined when calling saveApplianceUsingPATCH.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            '*/*'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json'
        ];
        const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected != undefined) {
            headers = headers.set('Content-Type', httpContentTypeSelected);
        }

        return this.httpClient.patch<EntityModelAppliance>(`${this.basePath}/exam/appliances/${encodeURIComponent(String(id))}`,
            body,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * saveAppliance
     * 
     * @param body body
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public saveApplianceUsingPOST(body: Appliance, observe?: 'body', reportProgress?: boolean): Observable<EntityModelAppliance>;
    public saveApplianceUsingPOST(body: Appliance, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<EntityModelAppliance>>;
    public saveApplianceUsingPOST(body: Appliance, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<EntityModelAppliance>>;
    public saveApplianceUsingPOST(body: Appliance, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (body === null || body === undefined) {
            throw new Error('Required parameter body was null or undefined when calling saveApplianceUsingPOST.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            '*/*'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json'
        ];
        const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected != undefined) {
            headers = headers.set('Content-Type', httpContentTypeSelected);
        }

        return this.httpClient.post<EntityModelAppliance>(`${this.basePath}/exam/appliances`,
            body,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * saveAppliance
     * 
     * @param body body
     * @param id id
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public saveApplianceUsingPUT(body: Appliance, id: number, observe?: 'body', reportProgress?: boolean): Observable<EntityModelAppliance>;
    public saveApplianceUsingPUT(body: Appliance, id: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<EntityModelAppliance>>;
    public saveApplianceUsingPUT(body: Appliance, id: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<EntityModelAppliance>>;
    public saveApplianceUsingPUT(body: Appliance, id: number, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (body === null || body === undefined) {
            throw new Error('Required parameter body was null or undefined when calling saveApplianceUsingPUT.');
        }

        if (id === null || id === undefined) {
            throw new Error('Required parameter id was null or undefined when calling saveApplianceUsingPUT.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            '*/*'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json'
        ];
        const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected != undefined) {
            headers = headers.set('Content-Type', httpContentTypeSelected);
        }

        return this.httpClient.put<EntityModelAppliance>(`${this.basePath}/exam/appliances/${encodeURIComponent(String(id))}`,
            body,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

}
