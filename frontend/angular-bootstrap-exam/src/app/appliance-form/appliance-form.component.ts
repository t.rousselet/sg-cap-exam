import { Component, OnInit, Inject } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApplianceControllerService, ApplianceDto } from 'src/typescript-angular-client';

@Component({
  selector: 'app-appliance-form',
  templateUrl: './appliance-form.component.html',
  styleUrls: ['./appliance-form.component.scss']
})

export class ApplianceFormComponent implements OnInit {
  public mode: 'update' | 'create';
  public appliance: ApplianceDto;
  public applianceForm: FormGroup;

  public loading: boolean = false;

  constructor(
    public applianceService: ApplianceControllerService,
    public dialogRef: MatDialogRef<ApplianceFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public formBuilder: FormBuilder,
    public snackBar: MatSnackBar,
  ) {
    this.mode = this.data.mode;
    this.appliance = this.data.appliance;
  }

  ngOnInit(): void {
    this.buildForm();

    if (this.appliance) this.applianceForm.patchValue(this.appliance);
  }

  public buildForm(): void {
    this.applianceForm = this.formBuilder.group({
      'id': [''],
      'brand': ['', Validators.required],
      'dateBought': [''],
      'model': ['', Validators.required],
      'serialNumber': ['', Validators.required],
      'status': ['']
    });
  }

  public submitForm(): void {
    if (this.applianceForm.invalid) return;

    this.loading = true;
    const editedAppliance: ApplianceDto = this.applianceForm.getRawValue() as ApplianceDto;
    editedAppliance.dateBought = editedAppliance.dateBought ? this.formatDateUTC(editedAppliance.dateBought) : null;
    if (this.mode === 'create'){
      this.applianceService.createApplianceUsingPOST(editedAppliance)
        .subscribe({
          next: (data: ApplianceDto) => {
            this.snackBar.open(`The appliance has been created successfully`, null, { duration: 5000 });
            this.dialogRef.close(data);
          },
          error: (error: HttpErrorResponse) => {
            if (error.status === 406){
              this.snackBar.open(`This Appliance Serial Number, Model and brand already exist`, null, { duration: 5000 });
            } else {
              this.snackBar.open(`An error has occured: ${error.message}`, null, { duration: 5000 });
            }
            this.loading = false;
          }
        }
        );
    } else if (this.mode === 'update'){
      this.applianceService.updateApplianceUsingPOST(editedAppliance)
        .subscribe({
          next: (data: ApplianceDto) => {
            this.snackBar.open(`The appliance has been updated successfully`, null, { duration: 5000 });
            this.dialogRef.close(data);
          },
          error: (error: HttpErrorResponse) => {
            if (error.status === 406){
              this.snackBar.open(`This Appliance Serial Number, Model and brand already exist`, null, { duration: 5000 });
            } else {
              this.snackBar.open(`An error has occured: ${error.message}`, null, { duration: 5000 });
            }
            this.loading = false;
          }
        }
        );
    }
  }

  public formatDateUTC(date: Date): Date {
    const formattedDate = new Date(date);
    const UTCDate = new Date(
      Date.UTC(formattedDate.getFullYear(), formattedDate.getMonth(), formattedDate.getDate()) - formattedDate.getTimezoneOffset());
    return UTCDate;
  }

  public clearDate(): void {
    this.applianceForm.get('dateBought').setValue('');
  }

}
