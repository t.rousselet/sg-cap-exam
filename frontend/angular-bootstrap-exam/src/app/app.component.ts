import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ApplianceFormComponent } from './appliance-form/appliance-form.component';
import { ApplianceControllerService, ApplianceDto } from 'src/typescript-angular-client';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public isInitializing: boolean = true;
  public isRefreshing: boolean = false;

  public allAppliances: ApplianceDto[] = [];
  public filteredAppliances: ApplianceDto[] = [];
  public filters: ApplianceDto;
  public allowedStatuses: string[] = ['old', 'unused', 'sold'];
  constructor(
    public applianceService: ApplianceControllerService,
    public snackBar: MatSnackBar,
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.applianceService.findAllApplianceUsingGET1()
      .subscribe({
        next: (appliances: ApplianceDto[]) => {
          this.allAppliances = appliances || [];
          this.isInitializing = false;
        },
        error: (error: HttpErrorResponse) => {
          this.snackBar.open(`An error has occured: ${error.message}`, null, {duration: 5000});
          this.isInitializing = false;
        }
      });
  }


  /*************/
  /* FILTERING */
  /*********** */
  public onFilterUpdate(filters: ApplianceDto): void {
    this.filteredAppliances = this.filterList(this.allAppliances, filters);
    this.filters = filters;
  }

  public filterList(total: ApplianceDto[], filters: ApplianceDto): ApplianceDto[] {
    return total.filter(item => {
      let isOccurence: boolean = true;

      for (var key in filters) {
        if (filters.hasOwnProperty(key)) {

          let itemValue = item[key] === null ? '' : item[key];
          let filterValue = filters[key] === null ? '' : filters[key];

          if (itemValue.toLowerCase().indexOf(filterValue.toLowerCase()) === -1) {
            isOccurence = false;
            break;
          }
        }
      }
      return isOccurence;
    })
  }

  /********************/
  /* DELETE APPLIANCE */
  /********************/

  public canBeDeleted(appliance: ApplianceDto): boolean {

    return this.allowedStatuses.includes(appliance.status);
  }

  public delete(appliance: ApplianceDto): void {
    if (confirm('Are you sure you want to delete this appliance ?')) {
      this.isRefreshing = true;

      this.applianceService.deleteApplianceUsingPOST(appliance)
        .subscribe({
          next: (data) => {
            this.allAppliances = this.removeApplianceFromList(appliance);
            this.filteredAppliances = this.filterList(this.allAppliances, this.filters);
            this.isRefreshing = false;
            this.snackBar.open('The appliance has been deleted successfully', null, { duration: 5000 });
          },
          error: (error: HttpErrorResponse) => {
            this.snackBar.open(`An error has occured: ${error.message}`, null, { duration: 5000 });
            this.isRefreshing = false;
          }
        })
    }
  }

  public removeApplianceFromList(appliance: ApplianceDto): ApplianceDto[] {
    return this.allAppliances.filter((item: ApplianceDto) => item.id !== appliance.id);
  }

  /******************/
  /* EDIT APPLIANCE */
  /******************/
  public openUpdateDialog(appliance: ApplianceDto): void {
    let dialogRef: MatDialogRef<ApplianceFormComponent> = this.dialog.open(ApplianceFormComponent, {
      data: {
        'mode': 'update',
        'appliance': appliance
      }
    });

    dialogRef.afterClosed().subscribe((data: ApplianceDto) => {
      if (data) {

        this.updateApplianceInList(data);
      }
    });
  }


  public updateApplianceInList(appliance: ApplianceDto): void {
    const index: number = this.allAppliances.findIndex(el => el.id === appliance.id );

    this.allAppliances[index] = appliance;
    this.filteredAppliances = this.filterList(this.allAppliances, this.filters);
  }

  /********************/
  /* CREATE APPLIANCE */
  /********************/
  public openCreateDialog(fromFilters: boolean = false) {
    let dialogRef: MatDialogRef<ApplianceFormComponent> = this.dialog.open(ApplianceFormComponent, {
      data: {
        'mode': 'create',
        'appliance': fromFilters ? this.filters : null
      }
    });

    dialogRef.afterClosed().subscribe((data: ApplianceDto) => {
      if (data) this.addApplianceInList(data);
    });
  }

  public addApplianceInList(appliance: ApplianceDto): void {
    this.allAppliances = [appliance, ...this.allAppliances];
    this.filteredAppliances = this.filterList(this.allAppliances, this.filters);
  }
}
