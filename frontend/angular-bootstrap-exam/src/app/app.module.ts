import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule } from '@angular/material/card';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTableModule } from '@angular/material/table';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';

import { AppComponent } from './app.component';

import { environment } from 'src/environments/environment';
import { HttpClientModule } from '@angular/common/http';

import { FilterPipe } from './filter.pipe';
import { FiltersComponent } from './filters/filters.component';
import { ApplianceFormComponent } from './appliance-form/appliance-form.component';
import { ApiModule, Configuration } from 'src/typescript-angular-client';


const config = new Configuration({ basePath: environment.basePath, apiKeys: {} });
export function configuration(): Configuration {
  return config;
}


@NgModule({
  declarations: [
    AppComponent,
    FilterPipe,
    FiltersComponent,
    ApplianceFormComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatToolbarModule,
    MatCardModule,
    HttpClientModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatTooltipModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    MatDialogModule,
    MatProgressBarModule,
    ApiModule.forRoot(configuration)
  ],
  providers: [
    FilterPipe,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
