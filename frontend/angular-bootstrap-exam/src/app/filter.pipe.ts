import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'filter' })
export class FilterPipe implements PipeTransform {
    transform(value: any[], filter: any): any[] {
        if (!filter) return value;

        return value.filter(item => {
            let isOccurence: boolean = true;

            for (var key in filter) {
                if (filter.hasOwnProperty(key)) {
                    if (item[key].toLowerCase().indexOf(filter[key].toLowerCase()) === -1) {
                        isOccurence = false;
                        break;
                    }
                }
            }

            return isOccurence;
        })

    }
}