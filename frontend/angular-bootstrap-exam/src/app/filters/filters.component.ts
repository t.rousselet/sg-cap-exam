import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ApplianceDto } from 'src/typescript-angular-client';


@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss']
})
export class FiltersComponent implements OnInit {

  @Output() updateFilters: EventEmitter<ApplianceDto> = new EventEmitter();

  public filtersForm: FormGroup;

  constructor(
    public formBuilder: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.filtersForm = this.formBuilder.group({
      'brand': [''],
      'dateBought': [''],
      'model': [''],
      'serialNumber': [''],
      'status': ['']
    });

    // Emit Default value
    this.updateFilters.emit(this.filtersForm.getRawValue());

    this.filtersForm.valueChanges.subscribe(data => {
      data.dateBought = data.dateBought ? this.convertDateToString(data.dateBought) : '';

      this.updateFilters.emit(data);
    });
  }


  public clearDate(): void {
    this.filtersForm.get('dateBought').setValue('');
  }


  // FORMAT STRING YYYY-MM-DD
  public convertDateToString(date: Date): string {
    let dd = String(date.getDate()).padStart(2, '0');
    var mm = String(date.getMonth() + 1).padStart(2, '0');
    var yyyy = date.getFullYear();

    return yyyy + '-' + mm + '-' + dd;
  }

}
