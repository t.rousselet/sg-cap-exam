package en.cap.exam.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import en.cap.exam.domain.Appliance;
import en.cap.exam.mapper.ApplianceMapper;
import en.cap.exam.service.Appliance.ApplianceService;
import en.cap.exam.viewmodel.ApplianceDto;

/*
 * This classe contain all the API for appliance
 */
@RestController
public class ApplianceController {
//	private static Logger LOG = LoggerFactory.getLogger(ApplianceController.class);
	
	@Autowired
	private ApplianceMapper mapper;
	
	@Autowired
	private ApplianceService applianceService;
	
	@RequestMapping(value="/findAllAppliance", method = RequestMethod.GET)
	public List<ApplianceDto> findAllAppliance() {
		List<Appliance> applianceList = applianceService.findAll();
		if(!applianceList.isEmpty()) {
			List<ApplianceDto> applianceListDto = applianceList.stream().map( appliance -> mapper.map(appliance)).collect(Collectors.toList());
			//(could have used : applianceList.forEach((appliance) -> applianceListDto.add(mapper.map(appliance)));)
			return applianceListDto;
		}
		return null;
	}
	
	@RequestMapping(value="/createAppliance",method = RequestMethod.POST)
	public ApplianceDto createAppliance(@RequestBody ApplianceDto applianceDto) {
//		LOG.info("INFO");
		var applianceToSave = mapper.map(applianceDto);
		var existingAppliance = applianceService.findBySerialNumberAndBrandAndModel(applianceDto.getSerialNumber(), applianceDto.getBrand(), applianceDto.getModel());
		if(existingAppliance == null || checkIfApplianceEmpty(existingAppliance)) {
			//appliance to create
			return mapper.map(applianceService.save(applianceToSave));
			
		} else {
			throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "This appliance already exist");
		}

	}
	
	@RequestMapping(value="/updateAppliance",method = RequestMethod.POST)
	public ApplianceDto updateAppliance(@RequestBody ApplianceDto applianceDto) {
//		LOG.info("INFO");
		var applianceUpdated = mapper.map(applianceDto);
		var existingAppliance = applianceService.findById(applianceDto.getId()).get();

		//check if serial Number, brand and model already exist for different one.
		var existingDifAppliance = applianceService.findBySerialNumberAndBrandAndModel(applianceDto.getSerialNumber(), applianceDto.getBrand(), applianceDto.getModel());
		if(existingDifAppliance != null && !checkIfApplianceEmpty(existingDifAppliance) &&
				existingDifAppliance.getId() != applianceDto.getId()){
			//this model, serialnumber and brand are already existing
			throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "This appliance brand, model and serial number info already exist");			
		}

		if(applianceUpdated.equals(existingAppliance)) {
			//no modification
			return applianceDto;
		}

		existingAppliance = mergeApplianceData(applianceUpdated,existingAppliance);

		return mapper.map(applianceService.save(existingAppliance));
	}
	
	@RequestMapping(value="/deleteAppliance",method = RequestMethod.POST)
	public void deleteAppliance(@RequestBody ApplianceDto applianceDto) {
		var existingAppliance = applianceService.findBySerialNumberAndBrandAndModel(applianceDto.getSerialNumber(), applianceDto.getBrand(), applianceDto.getModel());
		if (checkIfApplianceEmpty(existingAppliance)) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "This appliance doesn't exist");
		}
		applianceService.delete(existingAppliance);

	}
	
	//check and merge new info with existing ones.
	private Appliance mergeApplianceData(Appliance applianceToSave, Appliance existingAppliance) {
		if (!existingAppliance.getBrand().equals(applianceToSave.getBrand())) {
			existingAppliance.setBrand(applianceToSave.getBrand());
		}
		if (!existingAppliance.getModel().equals(applianceToSave.getModel())) {
			existingAppliance.setModel(applianceToSave.getModel());
		}
		if (!existingAppliance.getSerialNumber().equals(applianceToSave.getSerialNumber())) {
			existingAppliance.setSerialNumber(applianceToSave.getSerialNumber());
		}
		if(applianceToSave.getDateBought() == null || existingAppliance.getDateBought() == null ) {
			if (!(applianceToSave.getDateBought()== null && existingAppliance.getDateBought() == null)) {
				existingAppliance.setDateBought(applianceToSave.getDateBought());
			}		

		} else if (!existingAppliance.getDateBought().equals(applianceToSave.getDateBought())) {
			existingAppliance.setDateBought(applianceToSave.getDateBought());
		}
		if (!existingAppliance.getStatus().equals(applianceToSave.getStatus())) {
			existingAppliance.setStatus(applianceToSave.getStatus());
		}
		return existingAppliance;
	}
	
	//check if the Appliance object is empty
	private boolean checkIfApplianceEmpty(Appliance appliance) {
		if (appliance.getBrand() == null && appliance.getModel() == null 
				&& appliance.getDateBought() == null && appliance.getSerialNumber() == null && appliance.getStatus() == null) {
			return true;
		}
				return false;
	}
}
