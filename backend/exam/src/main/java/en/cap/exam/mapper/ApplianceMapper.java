package en.cap.exam.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import en.cap.exam.domain.Appliance;
import en.cap.exam.viewmodel.ApplianceDto;

/**
 * 
 * This class purpose is to map Appliance to ApplianceDto and vice versa
 *
 */
@Mapper(componentModel = "spring")
public interface ApplianceMapper {
	@Mapping(ignore = true, target = "id")
	@Mapping(ignore = true, target = "version")

	Appliance map(ApplianceDto applianceDto);
	
	ApplianceDto map(Appliance appliance);
}
