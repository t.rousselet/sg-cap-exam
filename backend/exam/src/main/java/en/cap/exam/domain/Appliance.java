package en.cap.exam.domain;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

/**
 * Appliance Entity
 */
@Entity
@Table(indexes = {@Index(columnList = "serialNumber,brand,model", unique = true)})
public class Appliance extends AbstractEntity {
	
	
	//Note : serial Number is defined as String as a serial number can start with an undefined quantity of 0; i.e : 0000146512 ; 0023456789
	private String serialNumber;
	private String brand;
	private String model;
	private String status;
	private LocalDate dateBought;
	
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public LocalDate getDateBought() {
		return dateBought;
	}
	public void setDateBought(LocalDate dateBought) {
		this.dateBought = dateBought;
	}

	
	
	
}
