package en.cap.exam.controller;

import java.time.LocalDate;

import javax.transaction.Transactional;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import en.cap.exam.domain.Appliance;
import en.cap.exam.service.Appliance.ApplianceService;

/**
 * This class is handling test API
 * The API will generate data to be used during test.
 */
@RestController
public class TestDataController {

	@Autowired
	private ApplianceService applianceService;
	
	@RequestMapping(value="/init-test-data",method = RequestMethod.POST)
	@Transactional
	public void createTestAppliance() {
		LocalDate date = LocalDate.of(2020, 1, 8);
		for (var i = 0; i < 100; i++) {
			var a = new Appliance();
			a.setBrand("Sony");
			a.setModel("TheModele");
			a.setSerialNumber(RandomStringUtils.randomAlphanumeric(9) +" "+ RandomStringUtils.randomAlphanumeric(9));
			a.setDateBought(date);
			a.setStatus("Used");
			applianceService.save(a);
		}
		
	}
}
