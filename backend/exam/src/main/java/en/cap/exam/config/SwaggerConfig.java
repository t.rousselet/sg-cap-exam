package en.cap.exam.config;

import java.util.Collections;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import springfox.documentation.PathProvider;
import springfox.documentation.schema.AlternateTypeRules;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.paths.DefaultPathProvider;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
/**
 * This is the swagger configuration for handling swagger
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
	@Bean
	public Docket api() {
		Docket docket = new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo())
				.ignoredParameterTypes(HttpServletRequest.class, HttpServletResponse.class, HttpSession.class)
				.alternateTypeRules(AlternateTypeRules.newRule(StreamingResponseBody.class, MultipartFile.class));

		docket = docket.pathMapping("/");

		return docket.pathProvider(absolutePathProvider()).select().build();
	}

	private PathProvider absolutePathProvider() {
		return new DefaultPathProvider();
	}

	@Bean
	public WebMvcConfigurer forwardToIndex() {
		return new WebMvcConfigurer() {
			@Override
			public void addViewControllers(ViewControllerRegistry registry) {
				String prefix = "";
				registry.addViewController("/").setViewName("redirect:" + prefix + "/swagger-ui.html");
				registry.addViewController("/api").setViewName("redirect:" + prefix + "/swagger-ui.html");
			}
		};
	}

	private ApiInfo apiInfo() {
		return new ApiInfo("REST API", "", "", "",
				new springfox.documentation.service.Contact("team", "", "bla@bla.com"), "", "",
				Collections.emptyList());
	}
}