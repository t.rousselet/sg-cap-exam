package en.cap.exam.service.Appliance;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import en.cap.exam.domain.Appliance;

public interface ApplianceService extends JpaRepository<Appliance,Long>{
	List<Appliance> findBySerialNumber(String serialNumber);
	List<Appliance> findByBrand(String brand);
	List<Appliance> findByModel(String model);
	List<Appliance> findByStatus(String status);
	List<Appliance> findByDateBought(LocalDate dateBought);
	Appliance findBySerialNumberAndBrandAndModelAndStatusAndDateBought(String serialNumber, String brand, String model, String status, LocalDate dateBought);
	Appliance findBySerialNumberAndBrandAndModel(String serialNumber, String brand, String model);
	
}
