package en.cap.exam.viewmodel;

import java.time.LocalDate;

import javax.validation.constraints.NotBlank;

public class ApplianceDto extends ApplianceDtoId {
	
	@NotBlank
	private String serialNumber;
	
	private String brand;
	
	private String model;
	
	private String status;
	
	private LocalDate dateBought;
	
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public LocalDate getDateBought() {
		return dateBought;
	}
	public void setDateBought(LocalDate dateBought) {
		this.dateBought = dateBought;
	}
	
	
	
	
}
