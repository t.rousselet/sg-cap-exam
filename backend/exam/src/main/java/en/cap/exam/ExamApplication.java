package en.cap.exam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


/**
 * SpringBootInit
 *
 */
@SpringBootApplication
@EnableJpaRepositories(basePackages = {"en.cap.exam.service", "en.cap.exam.controller"})
public class ExamApplication 
{
    public static void main( String[] args )
    {
    	SpringApplication.run(ExamApplication.class, args);
    }
}
