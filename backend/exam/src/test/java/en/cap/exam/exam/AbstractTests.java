package en.cap.exam.exam;

import java.net.URISyntaxException;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
/**
 * 
 * Abstract class that all JUnit Test must implements 
 * hosting config and several general useful funtions
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles({ "test" })
public abstract class AbstractTests {
    @Autowired
    private TestRestTemplate restTemplate;
    
    private HttpHeaders headers;
    
    @Before
    public void clearSession() {
    	this.headers = null;
    }
    
    public <T> ResponseEntity<T> get(String uri, Class<T> clazz) {
        HttpEntity<Void> requestEntity = new HttpEntity<>(null, headers);
        return restTemplate.exchange(uri, HttpMethod.GET, requestEntity, clazz);
    }
    
    public <T,P> ResponseEntity<T> post(String uri, P o, Class<T> clazz) {
        HttpEntity<P> requestEntity = new HttpEntity<>(o, headers);
        return restTemplate.exchange(uri, HttpMethod.POST, requestEntity, clazz);
    }    

	protected ResponseEntity<byte[]> getFile(String uri) {
	    restTemplate.getRestTemplate().getMessageConverters().add(new ByteArrayHttpMessageConverter());

	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    return restTemplate.exchange(uri, HttpMethod.GET, entity, byte[].class);
	}
     
    public <T,P> ResponseEntity<T> postFile(String uri, String ressourcePath, Class<T> clazz) throws URISyntaxException {
    	var map = new LinkedMultiValueMap<String, Object>();
	    map.add("data", new ClassPathResource(ressourcePath));

	    try {
		    headers.add("content-type", MediaType.MULTIPART_FORM_DATA_VALUE);
		    var requestEntity = new HttpEntity<>(map, headers);
		    return restTemplate.exchange(uri, HttpMethod.POST, requestEntity, clazz);
	    } finally {
			headers.remove("content-type");
		}
    }
    
    public <T> ResponseEntity<T> delete(String uri, Class<T> clazz) {
        HttpEntity<Void> requestEntity = new HttpEntity<>(null, headers);
        return restTemplate.exchange(uri, HttpMethod.DELETE, requestEntity, clazz);
    }
    


}
