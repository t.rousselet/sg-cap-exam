package en.cap.exam.exam;
import java.time.LocalDate;

import javax.transaction.Transactional;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;

import en.cap.exam.domain.Appliance;
import en.cap.exam.mapper.ApplianceMapper;
import en.cap.exam.service.Appliance.ApplianceService;
import en.cap.exam.viewmodel.ApplianceDto;

/**
 * Note : Tests won't work if the app is launched already.
 */
@Transactional
public class AppTest extends AbstractTests
{
	
	@Autowired
	private ApplianceService applianceService;
	
	@Autowired
	private ApplianceMapper mapper;

	
	
   @Test
   @Transactional
   @Rollback(true)
    public void testCreate()
    {
	    LocalDate date = LocalDate.of(2020, 2, 8);
		var applianceDto = new ApplianceDto();
		applianceDto.setBrand("testBrand");
		applianceDto.setModel("testModele");
		applianceDto.setSerialNumber(RandomStringUtils.randomAlphanumeric(9) +" "+ RandomStringUtils.randomAlphanumeric(9));
		applianceDto.setDateBought(date);
		applianceDto.setStatus("testStatus");
		var applianceToSave = mapper.map(applianceDto);
		var existingAppliance = applianceService.findBySerialNumberAndBrandAndModel(applianceDto.getSerialNumber(), applianceDto.getBrand(), applianceDto.getModel());
		if(existingAppliance == null || checkIfApplianceEmpty(existingAppliance)) {
			//appliance to create
			var result = mapper.map(applianceService.save(applianceToSave));
			applianceDto.setId(result.getId());
			assert result.getBrand().equals(applianceDto.getBrand());
			assert result.getId() == applianceDto.getId();
			assert result.getModel().equals(applianceDto.getModel());
			assert result.getDateBought().equals(applianceDto.getDateBought());
			assert result.getStatus().equals(applianceDto.getStatus());
			assert result.getSerialNumber().equals(applianceDto.getSerialNumber());
			
		} else {
			assert true;
		}

    }
    @Test    
    @Transactional
    @Rollback(true)
    public void testUpdate()
    {

		LocalDate date = LocalDate.of(2020, 2, 8);
		var appliance = new Appliance();
		appliance.setBrand("testBrand");
		appliance.setModel("testModele");
		appliance.setSerialNumber(RandomStringUtils.randomAlphanumeric(9) +" "+ RandomStringUtils.randomAlphanumeric(9));
		appliance.setDateBought(date);
		appliance.setStatus("testStatus");
		var insertedAppliance = applianceService.save(appliance);
		
		var applianceDto = new ApplianceDto();
		applianceDto.setId(insertedAppliance.getId());
		applianceDto.setBrand("testBrand");
		applianceDto.setModel("testModele");
		applianceDto.setSerialNumber(RandomStringUtils.randomAlphanumeric(9) +" "+ RandomStringUtils.randomAlphanumeric(9));
		applianceDto.setDateBought(date);
		applianceDto.setStatus("testStatus");
		
		var appliance2 = new Appliance();
		appliance2.setBrand("testBrand");
		appliance2.setModel("testModele");
		appliance2.setSerialNumber(RandomStringUtils.randomAlphanumeric(9) +" "+ RandomStringUtils.randomAlphanumeric(9));
		appliance2.setDateBought(date);
		appliance2.setStatus("testStatus");
		var insertedAppliance2 = applianceService.save(appliance2);
		

		
		var applianceToSave = mapper.map(applianceDto);
		var existingAppliance = applianceService.findById(applianceDto.getId()).get();
		var existingDifAppliance = applianceService.findBySerialNumberAndBrandAndModel(applianceDto.getSerialNumber(), applianceDto.getBrand(), applianceDto.getModel());

		if(existingDifAppliance != null && !checkIfApplianceEmpty(existingDifAppliance) &&
				existingDifAppliance.getId() != applianceDto.getId()){
			//this model, serialnumber and brand are already existing
			assert true;			

		}

		if(applianceToSave.equals(existingAppliance)) {
			//no modification
			assert true;
		}

		existingAppliance = mergeApplianceData(applianceToSave,existingAppliance);

		var result = mapper.map(applianceService.save(existingAppliance));
		assert result.getBrand().equals(applianceDto.getBrand());
		assert result.getId() == applianceDto.getId();
		assert result.getModel().equals(applianceDto.getModel());
		assert result.getDateBought().equals(applianceDto.getDateBought());
		assert result.getStatus().equals(applianceDto.getStatus());
		assert result.getSerialNumber().equals(applianceDto.getSerialNumber());
		
			
		
    }
	
	@Test
	@Transactional
   	@Rollback(true)
	public void testDelete()
	{
		LocalDate date = LocalDate.of(2020, 2, 8);
		var appliance = new Appliance();
		appliance.setBrand("testBrand");
		appliance.setModel("testModele");
		appliance.setSerialNumber(RandomStringUtils.randomAlphanumeric(9) +" "+ RandomStringUtils.randomAlphanumeric(9));
		appliance.setDateBought(date);
		appliance.setStatus("testStatus");
		var insertedAppliance = applianceService.save(appliance);
		var existingAppliance = applianceService.findBySerialNumberAndBrandAndModel(insertedAppliance.getSerialNumber(), insertedAppliance.getBrand(), insertedAppliance.getModel());
		assert existingAppliance == insertedAppliance;
		applianceService.delete(existingAppliance);
	}
	
	private boolean checkIfApplianceEmpty(Appliance appliance) {
		if (appliance.getBrand() == null && appliance.getModel() == null 
				&& appliance.getDateBought() == null && appliance.getSerialNumber() == null && appliance.getStatus() == null) {
			return true;
		}
				return false;
	}
	
	//check and merge new info with existing ones.
	private Appliance mergeApplianceData(Appliance applianceToSave, Appliance existingAppliance) {
		if (!existingAppliance.getBrand().equals(applianceToSave.getBrand())) {
			existingAppliance.setBrand(applianceToSave.getBrand());
		}
		if (!existingAppliance.getModel().equals(applianceToSave.getModel())) {
			existingAppliance.setModel(applianceToSave.getModel());
		}
		if (!existingAppliance.getDateBought().equals(applianceToSave.getDateBought())) {
			existingAppliance.setDateBought(applianceToSave.getDateBought());
		}
		if (!existingAppliance.getSerialNumber().equals(applianceToSave.getSerialNumber())) {
			existingAppliance.setSerialNumber(applianceToSave.getSerialNumber());
		}
		if (!existingAppliance.getStatus().equals(applianceToSave.getStatus())) {
			existingAppliance.setStatus(applianceToSave.getStatus());
		}
		return existingAppliance;
	}
}
